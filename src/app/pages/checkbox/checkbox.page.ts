import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-checkbox",
  templateUrl: "./checkbox.page.html",
  styleUrls: ["./checkbox.page.scss"],
})
export class CheckboxPage implements OnInit {
  data = [
    {
      name: "primary",
      selected: false,
    },
    {
      name: "secondary",
      selected: true,
    },
    {
      name: "success",
      selected: false,
    },
    {
      name: "tertiary",
      selected: false,
    },
  ];
  constructor() {}

  ngOnInit() {}

  onClick(item) {
    console.log(item);
  }
}
