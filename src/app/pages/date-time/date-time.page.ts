import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-date-time",
  templateUrl: "./date-time.page.html",
  styleUrls: ["./date-time.page.scss"],
})
export class DateTimePage implements OnInit {
  backdropDismiss: false;
  fechaNacimiento: Date = new Date();
  customPickerOptions;
  customDate;
  constructor() {}

  ngOnInit() {
    //console.log(this.fechaNacimiento);

    this.customPickerOptions = {
      buttons: [
        {
          text: "Save",
          handler: (evento) => {
            console.log("Clicked Save!");
            console.log(evento.month);
          },
        },
        {
          text: "Log",
          handler: () => {
            console.log("Clicked Log. Do not Dismiss.");
            return false;
          },
        },
      ],
    };
  }

  cambioFecha(event) {
    console.log("ionChange", event);
    console.log("date", new Date(event.detail.value));
  }
}
