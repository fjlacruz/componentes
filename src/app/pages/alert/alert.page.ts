import { Component, OnInit } from "@angular/core";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-alert",
  templateUrl: "./alert.page.html",
  styleUrls: ["./alert.page.scss"],
})
export class AlertPage implements OnInit {
  titulo: string;
  constructor(public alertController: AlertController) {}

  ngOnInit() {}
  async presentAlert() {
    const alert = await this.alertController.create({
      backdropDismiss: false,
      cssClass: "my-custom-class",
      header: "Alert",
      subHeader: "Subtitle",
      message: "This is an alert message.",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Cancel");
          },
        },
        {
          text: "Ok",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Ok");
          },
        },
      ],
    });

    await alert.present();
  }
  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Prompt!",
      inputs: [
        {
          name: "txtNombre",
          type: "text",
          placeholder: "Placeholder",
        },
        {
          name: "txtApellido",
          type: "text",
          placeholder: "Placeholder",
        },
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel");
          },
        },
        {
          text: "Ok",
          handler: (data) => {
            console.log("Confirm Ok", data);
            this.titulo = data.txtNombre;
            console.log(this.titulo);
          },
        },
      ],
    });

    await alert.present();
  }
}
