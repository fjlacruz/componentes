import { InicioPage } from "./../inicio/inicio.page";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActionSheetController } from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-action-sheet",
  templateUrl: "./action-sheet.page.html",
  styleUrls: ["./action-sheet.page.scss"],
})
export class ActionSheetPage implements OnInit {
  [x: string]: any;
  pushPage: typeof InicioPage;
  constructor(
    private actionSheetCtrl: ActionSheetController,
    private router: Router
  ) {}

  ngOnInit() {}

  async presentActionSheet() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: "Albums",
      backdropDismiss: false,
      cssClass: "my-custom-class",
      buttons: [
        {
          text: "Delete",
          role: "destructive",
          icon: "trash",

          handler: () => {
            this.redireccion();
          },
        },
        {
          text: "Share",
          icon: "share",
          handler: () => {
            console.log("Share clicked");
          },
        },
        {
          text: "Play (open modal)",
          icon: "caret-forward-circle",
          handler: () => {
            console.log("Play clicked");
          },
        },
        {
          text: "Favorite",
          icon: "heart",
          handler: () => {
            console.log("Favorite clicked");
          },
        },
        {
          text: "Cancel",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    await actionSheet.present();
  }
  redireccion() {
    console.log("Delete clicked");
    this.router.navigate(["/inicio"]);
  }
}
