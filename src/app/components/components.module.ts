import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeadersComponent } from "./headers/headers.component";
import { IonicModule } from "@ionic/angular";
import { MenuComponent } from "./menu/menu.component";
@NgModule({
  declarations: [HeadersComponent, MenuComponent],
  exports: [HeadersComponent, MenuComponent],
  imports: [CommonModule, IonicModule, RouterModule],
})
export class ComponentsModule {}
