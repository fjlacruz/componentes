import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Componente } from "../interfaces/interfaces";

@Injectable({
  providedIn: "root",
})
export class DataService {
  constructor(private http: HttpClient) {}

  getMenuOption() {
    return this.http.get<Componente[]>("/assets/menu.json");
  }
}
